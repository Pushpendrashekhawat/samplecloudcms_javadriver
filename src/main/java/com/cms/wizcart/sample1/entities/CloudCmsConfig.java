package com.cms.wizcart.sample1.entities;

public enum CloudCmsConfig {
	projectName("Sevice Marketplace"),
	repoName("Sevice Marketplace 'content' repository"),
	branchName("master");

	private String cloudCmsConfig;

	public String getCloudCmsConfig() {
		return cloudCmsConfig;
	}

	public void setCloudCmsConfig(String cloudCmsConfig) {
		this.cloudCmsConfig = cloudCmsConfig;
	}

	private CloudCmsConfig(String cloudCmsConfig) {
		this.cloudCmsConfig = cloudCmsConfig;
	}

}
