package com.cms.wizcart.sample1.entities;

import java.util.Map;

public class EmailNotificationPayload {

	private Map<String, Object> notificationData;

	public Map<String, Object> getNotificationData() {
		return notificationData;
	}

	public void setNotificationData(Map<String, Object> dataMap) {
		this.notificationData = dataMap;
	}

}
