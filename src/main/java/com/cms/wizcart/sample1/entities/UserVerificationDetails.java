package com.cms.wizcart.sample1.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserVerificationDetails {
	@JsonProperty("Username")
	private String username;
	@JsonProperty("VerificationCode")
	private Integer verificationCode;
	@JsonProperty("RemainingRetries")
	private Integer remainingRetries;
	@JsonProperty("IsVerified")
	private String isVerified;

	@JsonProperty("_type")
	private String type;

	@JsonProperty("_doc")
	private String id;
	@JsonProperty("_qname")
	private String qname;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(Integer verificationCode) {
		this.verificationCode = verificationCode;
	}

	public Integer getRemainingRetries() {
		return remainingRetries;
	}

	public void setRemainingRetries(Integer remainingRetries) {
		this.remainingRetries = remainingRetries;
	}

	public String getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(String isVerified) {
		this.isVerified = isVerified;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
