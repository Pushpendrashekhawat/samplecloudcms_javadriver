package com.cms.wizcart.sample1.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDetails {

	@JsonProperty("FirstName")
	private String firstName;
	@JsonProperty("LastName")
	private String lastname;
	@JsonProperty("Username")
	private String username;
	@JsonProperty("Password")
	private String password;
	@JsonProperty("NotificationConsent")
	private NotificationConsent notificationConsent;
	@JsonProperty("Email")
	private String email;
	@JsonProperty("PhoneNo")
	private String phoneNumber;
	@JsonProperty("Title")
	private String title;

	@JsonProperty("_type")
	private String type;

	@JsonProperty("_doc")
	private String id;
	@JsonProperty("_qname")
	private String qname;

	public UserDetails() {

	}

	public UserDetails(String firstName, String lastname, String username, String password, NotificationConsent notificationConsent, String email, String phoneNumber, String type, String title) {
		this.firstName = firstName;
		this.lastname = lastname;
		this.username = username;
		this.password = password;
		this.notificationConsent = notificationConsent;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.type = type;
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public NotificationConsent getNotificationConsent() {
		return notificationConsent;
	}

	public void setNotificationConsent(NotificationConsent notificationConsent) {
		this.notificationConsent = notificationConsent;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
