package com.cms.wizcart.sample1.entities;

public enum ContentTypes {

	Customer("custom:customer"),
	UserVerification("custom:userverification");

	private String contentType;

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	private ContentTypes(String contentType) {
		this.contentType = contentType;
	}

}
