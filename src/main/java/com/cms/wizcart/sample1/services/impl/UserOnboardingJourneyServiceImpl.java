package com.cms.wizcart.sample1.services.impl;

import org.gitana.platform.client.branch.Branch;
import org.gitana.platform.client.node.BaseNode;
import org.gitana.platform.client.node.Node;
import org.gitana.platform.support.QueryBuilder;
import org.gitana.platform.support.ResultMap;
import org.gitana.platform.support.ResultMapImpl;
import org.gitana.util.JsonUtil;

import com.cms.wizcart.sample1.entities.ContentTypes;
import com.cms.wizcart.sample1.entities.UserDetails;
import com.cms.wizcart.sample1.services.UserOnboardingJourneyService;
import com.cms.wizcart.sample1.utility.CloudCmsSetup;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class UserOnboardingJourneyServiceImpl implements UserOnboardingJourneyService {

	Branch master = CloudCmsSetup.getBranch();

	@Override
	public boolean RegisterCustomer(UserDetails userDetails) throws Exception {
		if(master != null) {
		UserServiceImpl userServiceImpl = new UserServiceImpl();
		ResultMap<BaseNode> resultUsernameQuery = userServiceImpl.findByUsername(userDetails.getUsername());
		if (!resultUsernameQuery.isEmpty()) {
			throw new Exception("userrname already exists");
		}
		ResultMap<BaseNode> resultEmailQuery = userServiceImpl.findByEmail(userDetails.getEmail());
		if (!resultEmailQuery.isEmpty()) {
			throw new Exception("Email already in use");
		}

		ObjectNode object = JsonUtil.createObject();
		object.put("FirstName", userDetails.getFirstName());
		object.put("LastName", userDetails.getLastname());
		object.put("Username", userDetails.getUsername());
		object.put("Password", userDetails.getPassword());
		object.put("NotificationConsent", userDetails.getNotificationConsent()
				.name()
				.toString());
		object.put("Email", userDetails.getEmail());
		object.put("PhoneNo", userDetails.getPhoneNumber());
		object.put("_type", ContentTypes.Customer.getContentType()); //TODO:Should this contain a fixed value? Where should we keep it.( Fixed, needs to be approved)
		object.put("title", userDetails.getTitle());
		Node node = (Node) master.createNode(object);

		return node.isSaved();
		}
		return false;
//		else {
//			throw new Exception("Error fetching CMS configurations" );
//		}
	}

	@Override
	public ResultMap<BaseNode> AuthenticateCustomer(String username, String password) {
		ResultMap<BaseNode> resultJson = new ResultMapImpl<BaseNode>();
		if(master != null) {
		ObjectNode query = QueryBuilder.start("Username")
				.is(username)
				.and("Password")
				.is(password)
				.get();
		resultJson = master.queryNodes(query);
		return resultJson;
		}
		return resultJson;
		//TODO: Should we throw an error instead of empty map? If yes, need to change this at other places also
	}

}
