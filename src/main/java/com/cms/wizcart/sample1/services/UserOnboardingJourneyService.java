package com.cms.wizcart.sample1.services;

import org.gitana.platform.client.node.BaseNode;
import org.gitana.platform.support.ResultMap;

import com.cms.wizcart.sample1.entities.UserDetails;

public interface UserOnboardingJourneyService {

	public ResultMap<BaseNode> AuthenticateCustomer(String username, String password);

	public boolean RegisterCustomer(UserDetails userDetails) throws Exception;
}
