package com.cms.wizcart.sample1.services;

public interface VerificationCodeService {

	public Boolean sendVerificationCode(String firstName, String email);

	public boolean validateCode(Integer verificationCode, String username);

}
