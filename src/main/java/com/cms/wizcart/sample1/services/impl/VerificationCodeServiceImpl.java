package com.cms.wizcart.sample1.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.gitana.platform.client.branch.Branch;
import org.gitana.platform.client.node.Node;
import org.gitana.util.JsonUtil;

import com.cms.wizcart.sample1.entities.ContentTypes;
import com.cms.wizcart.sample1.entities.EmailNotificationPayload;
import com.cms.wizcart.sample1.entities.IsVerified;
import com.cms.wizcart.sample1.entities.UserVerificationDetails;
import com.cms.wizcart.sample1.services.UserService;
import com.cms.wizcart.sample1.services.VerificationCodeService;
import com.cms.wizcart.sample1.utility.CloudCmsSetup;
import com.cms.wizcart.sample1.utility.EmailUtility;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class VerificationCodeServiceImpl implements VerificationCodeService {

	Branch master = CloudCmsSetup.getBranch();

	@Override
	public Boolean sendVerificationCode(String username, String toEmail) {
		Boolean isEmailSent = false;
		try {
			EmailUtility emailUtility = new EmailUtility();
			var verificationCode = generateCode();

			ObjectNode object = JsonUtil.createObject();
			object.put("Username", username);
			object.put("VerificationCode", verificationCode);
			object.put("RemainingRetries", 3); // TODO: remove harcoded value
			object.put("IsVerified", IsVerified.no.toString()); // TODO: make
																// enum
			object.put("_type", ContentTypes.UserVerification.getContentType());
			Node node = (Node) master.createNode(object);

			// TODO: Make separate builder for creating notification payload
			EmailNotificationPayload notificationPayload = new EmailNotificationPayload();
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put("verificationCode", verificationCode);
			notificationPayload.setNotificationData(dataMap);

			String emailBody = emailUtility.getEmailBodyFromTemplate(notificationPayload);
			// System.out.println("EMAIL BODY" + emailBody);
			emailUtility.sendEmail(toEmail, "", emailBody, "");
			isEmailSent = true;
			return isEmailSent;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isEmailSent;

	}

	private static Integer generateCode() {
		int randomNumber = (int) Math.floor(Math.random() * (900000 - 100000 + 1)) + 100000;
		return randomNumber;
	}

	@Override
	public boolean validateCode(Integer verificationCode, String username) {
		UserService user = new UserServiceImpl();
		if (master != null) {
			UserVerificationDetails userVerificationDetails = user.findByUsernameInUserVerification(username);

			if (userVerificationDetails == null) {
				return false;
			}
			if (userVerificationDetails.getIsVerified()
					.equals(IsVerified.yes.toString())) {
				return false;
			}
			if (userVerificationDetails.getRemainingRetries() == 0) {
				return false;
			}

			if (verificationCode.compareTo(userVerificationDetails.getVerificationCode()) == 0) {

				userVerificationDetails.setIsVerified(IsVerified.yes.toString());
				Node node = (Node) master.readNode(userVerificationDetails.getId());
				node.set("IsVerified", userVerificationDetails.getIsVerified());
				node.update();
				return true;
			} else {
				userVerificationDetails.setRemainingRetries(userVerificationDetails.getRemainingRetries() - 1);
				Node node = (Node) master.readNode(userVerificationDetails.getId());
				node.set("RemainingRetries", userVerificationDetails.getRemainingRetries());
				node.update();
				return false;
			}
		}
		return false;
	}
}
