package com.cms.wizcart.sample1.services;

import java.util.List;

import org.gitana.platform.client.Gitana;
import org.gitana.platform.client.branch.Branch;
import org.gitana.platform.client.node.Node;
import org.gitana.platform.client.platform.Platform;
import org.gitana.platform.client.platform.PlatformDataStore;
import org.gitana.platform.client.project.Project;
import org.gitana.platform.client.repository.Repository;
import org.gitana.platform.support.QueryBuilder;
import org.gitana.platform.support.ResultMap;
import org.gitana.util.JsonUtil;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Hello world!
 *
 */
public class UserOnboardingJourney {
	// private final static String clientKey =
	// "7258bc43-9e1f-4b9e-b451-8c6e73e56e2c";
	// private final static String clientSecret =
	// "BM5UQPX3TaFxLuyJMDiK0F7n31GgIi/7MG7ez+zU669zVGn+hNzHMUrZ3pW7Vu6UwK/OkG0LiUMgLHq0msKQXLfL8nMaJWT8zXYAG8MYX6s=";
	// private final static String username =
	// "9fc63303-94a4-45f5-9a05-8f5843f09b04";
	// private final static String password =
	// "smtWfpTHa3iSkwwrw9SI0jKi16ORV9Qufb7ehaXHgDE5PfHdpih9U3dHf6iKsIsZInULh5h3Sx25N/ennbEQIxVg7LUpW2I4mv3FCWfkVhM=";

	private final static String projectName = "Sevice Marketplace";
	private final static String repoName = "Sevice Marketplace 'content' repository";
	private final static String branchName = "master";

	public boolean RegisterCustomer(String firstName, String lastname, String username, String password, String notificationConsent, String email, String phoneNumber, String type) throws Exception {

		Branch master = getBranch();

		ResultMap resultUsernameQuery = findUsername(username);
		ResultMap resultEmailQuery = findEmail(email);

		if (!resultUsernameQuery.isEmpty()) {

			System.out.println("Username already exists:" + username);
			throw new Exception("userrname already exists");
		}

		if (!resultEmailQuery.isEmpty()) {
			throw new Exception("Email already in use");
		}

		ObjectNode object = JsonUtil.createObject();
		object.put("FirstName", firstName);
		object.put("LastName", lastname);
		object.put("Username", username);
		object.put("Password", password);
		object.put("NotificationConsent", notificationConsent);
		object.put("Email", email);
		object.put("PhoneNo", phoneNumber);
		object.put("_type", type);
		Node node = (Node) master.createNode(object);
		System.out.println("NOde created:" + node);
		return node.isSaved();

	}

	public ResultMap AuthenticateCustomer(String username, String password) {

		Branch master = getBranch();

		ObjectNode query = QueryBuilder.start("Username")
				.is(username)
				.and("Password")
				.is(password)
				.get();

		ResultMap resultJson = master.queryNodes(query);

		return resultJson;

	}

	public ResultMap findUsername(String username) {

		Branch master = getBranch();

		ObjectNode query = QueryBuilder.start("Username")
				.is(username)
				.get();

		ResultMap resultJson = master.queryNodes(query);

		return resultJson;
	}

	public ResultMap findEmail(String email) {

		Branch master = getBranch();

		ObjectNode query = QueryBuilder.start("Email")
				.is(email)
				.get();

		ResultMap resultJson = master.queryNodes(query);

		return resultJson;
	}

	private static Platform getConfiguration() {
		// Gitana gitana = new Gitana(clientKey, clientSecret);
		// Platform platform = gitana.authenticate(username, password);

		Gitana gitana = new Gitana();
		Platform platform = gitana.authenticate();
		return platform;
	}

	private static Project getProject(Platform platform, String projectname) {

		Project myProject = null;
		List<Project> projectList = platform.listProjects()
				.asList();
		for (Project project : projectList) {
			if (project.getTitle()
					.equalsIgnoreCase(projectname)) {
				myProject = project;
				System.out.println("Project Name:" + myProject.getTitle());
				break;
			}
			// System.out.println("Project title: " + project.getTitle());
		}
		return myProject;
	}

	private static Repository getReposoitory(Project myProject, String repoName) {

		Repository myRepository = null;
		List<PlatformDataStore> datastores = myProject.getStack()
				.listDataStores()
				.asList();
		for (PlatformDataStore dataStore : datastores) {
			if (dataStore.getTitle()
					.equalsIgnoreCase(repoName)) {
				// this is the content repository for this project
				myRepository = (Repository) dataStore;

				System.out.println("Repository title:" + myRepository.getTitle());
				// System.out.println("Myrepository :" + myRepository);
				break;
			}

			System.out.println("datastore Title:" + dataStore.getTitle());

		}
		return myRepository;
	}

	public Branch getBranch() {

		Platform platform = getConfiguration();

		Project myProject = getProject(platform, projectName);

		Repository currentRepo = getReposoitory(myProject, repoName);

		Branch master = currentRepo.readBranch(branchName);

		return master;
	}

}
