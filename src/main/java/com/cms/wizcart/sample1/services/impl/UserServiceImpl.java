package com.cms.wizcart.sample1.services.impl;

import org.gitana.platform.client.branch.Branch;
import org.gitana.platform.client.node.BaseNode;
import org.gitana.platform.services.node.NodeBuilder;
import org.gitana.platform.support.Pagination;
import org.gitana.platform.support.QueryBuilder;
import org.gitana.platform.support.ResultMap;
import org.gitana.platform.support.ResultMapImpl;
import org.gitana.platform.support.Sorting;

import com.cms.wizcart.sample1.entities.ContentTypes;
import com.cms.wizcart.sample1.entities.UserDetails;
import com.cms.wizcart.sample1.entities.UserVerificationDetails;
import com.cms.wizcart.sample1.services.UserService;
import com.cms.wizcart.sample1.utility.CloudCmsSetup;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class UserServiceImpl implements UserService {

	Branch master = CloudCmsSetup.getBranch();

	@Override
	public ResultMap<BaseNode> findByUsername(String username) {
		ResultMap<BaseNode> resultJson = new ResultMapImpl<BaseNode>();
		if (master != null) {
			ObjectNode query = QueryBuilder.start("Username")
					.is(username)
					.get();

			resultJson = master.queryNodes(query);
			return resultJson;
		}
		return resultJson;

	}

	@Override
	public UserDetails findByUsernameInUsers(String username) {
		UserDetails userDetails = null;
		if (master != null) {
			ObjectMapper objMapper = new ObjectMapper();
			ObjectNode _fields = NodeBuilder.start("FirstName")
					.is(1)
					.and("LastName")
					.is(1)
					.and("Username")
					.is(1)
					.and("Password")
					.is(1)
					.and("NotificationConsent")
					.is(1)
					.and("Email")
					.is(1)
					.and("PhoneNo")
					.is(1)
					.and("_qname")
					.is(1)
					.and("_type")
					.is(1)
					.get();

			ObjectNode query = NodeBuilder.start("Username")
					.is(username)
					.hasType(ContentTypes.Customer.getContentType())
					.and("_fields")
					.is(_fields)
					.get();
			ResultMap<BaseNode> resultJson = master.queryNodes(query);
			if (resultJson.isEmpty()) {
				return null;
			}
			ObjectNode node = resultJson.asList()
					.get(0)
					.toJSON();
			userDetails = objMapper.convertValue(node, UserDetails.class);
			return userDetails;
		}
		return userDetails;
	}

	@Override
	public ResultMap<BaseNode> findByEmail(String email) {

		ResultMap<BaseNode> resultJson = new ResultMapImpl<BaseNode>();
		if (master != null) {
			ObjectNode query = QueryBuilder.start("Email")
					.is(email)
					.get();
			resultJson = master.queryNodes(query);

			return resultJson;
		}
		return resultJson;
	}

	@Override
	public UserVerificationDetails findByUsernameInUserVerification(String username) {
		UserVerificationDetails userVerificationDetails = null;
		if (master != null) {
			ObjectMapper objMapper = new ObjectMapper();
			ObjectNode _fields = NodeBuilder.start("Username")
					.is(1)
					.and("VerificationCode")
					.is(1)
					.and("RemainingRetries")
					.is(1)
					.and("IsVerified")
					.is(1)
					.and("_qname")
					.is(1)
					.and("_type")
					.is(1)
					.get();

			ObjectNode query = NodeBuilder.start("Username")
					.is(username)
					.hasType(ContentTypes.UserVerification.getContentType())
					.and("_fields")
					.is(_fields)
					.get();

			Pagination pagination1 = new Pagination();
			Sorting sorting = new Sorting();
			sorting.addSortDescending("_system.created_on.ms");
			pagination1.setSorting(sorting);
			ResultMap<BaseNode> resultJson = master.queryNodes(query, pagination1);
			if (resultJson.isEmpty()) {
				return null;
			}
			ObjectNode node = resultJson.asList()
					.get(0)
					.toJSON();
			userVerificationDetails = objMapper.convertValue(node, UserVerificationDetails.class);
			System.out.println(userVerificationDetails.getVerificationCode());
			return userVerificationDetails;
		}
		return userVerificationDetails;
	}

	@Override
	public ResultMap<BaseNode> findByUsernameAndContentType(String username, String typeName) {

		// TODO Auto-generated method stub
		return null;
	}

}
