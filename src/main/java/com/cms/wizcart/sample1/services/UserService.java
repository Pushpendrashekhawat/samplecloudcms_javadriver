package com.cms.wizcart.sample1.services;

import org.gitana.platform.client.node.BaseNode;
import org.gitana.platform.support.ResultMap;

import com.cms.wizcart.sample1.entities.UserDetails;
import com.cms.wizcart.sample1.entities.UserVerificationDetails;

public interface UserService {

	public ResultMap<BaseNode> findByUsername(String username);

	public ResultMap<BaseNode> findByEmail(String email);

	public UserVerificationDetails findByUsernameInUserVerification(String username);

	public ResultMap<BaseNode> findByUsernameAndContentType(String username, String typeName);

	public UserDetails findByUsernameInUsers(String username);

	// TODO: Check if querying by contenType Name be made generic rather than
	// making making different methods for different content types
}
