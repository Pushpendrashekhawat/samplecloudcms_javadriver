package com.cms.wizcart.sample1.utility;

import java.util.List;

import org.gitana.platform.client.Gitana;
import org.gitana.platform.client.branch.Branch;
import org.gitana.platform.client.platform.Platform;
import org.gitana.platform.client.platform.PlatformDataStore;
import org.gitana.platform.client.project.Project;
import org.gitana.platform.client.repository.Repository;

import com.cms.wizcart.sample1.entities.CloudCmsConfig;

public class CloudCmsSetup {

	// TODO: Change these contants to ENUMS OR should we put them in properties
	// file?
	// private final static String projectName = "Sevice Marketplace";
	// private final static String repoName = "Sevice Marketplace 'content'
	// repository";
	// private final static String branchName = "master";

	public static Branch getBranch() {

		Platform platform = getConfiguration();
		if (platform != null) {

			Project myProject = getProject(platform, CloudCmsConfig.projectName.getCloudCmsConfig());

			Repository currentRepo = getReposoitory(myProject, CloudCmsConfig.repoName.getCloudCmsConfig());

			Branch master = currentRepo.readBranch(CloudCmsConfig.branchName.getCloudCmsConfig());

			return master;
		}

		return null;
	}

	public static Platform getConfiguration() {
		Platform platform = null;
		try {
			Gitana gitana = new Gitana();
			platform = gitana.authenticate();
			return platform;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return platform;
	}

	private static Project getProject(Platform platform, String projectname) {
		Project myProject = null;
		List<Project> projectList = platform.listProjects()
				.asList();
		for (Project project : projectList) {
			if (project.getTitle()
					.equalsIgnoreCase(projectname)) {
				myProject = project;
				System.out.println("Project Name:" + myProject.getTitle());
				break;
			}
			// System.out.println("Project title: " + project.getTitle());
		}
		return myProject;
	}

	private static Repository getReposoitory(Project myProject, String repoName) {

		Repository myRepository = null;
		List<PlatformDataStore> datastores = myProject.getStack()
				.listDataStores()
				.asList();
		for (PlatformDataStore dataStore : datastores) {
			if (dataStore.getTitle()
					.equalsIgnoreCase(repoName)) {
				// this is the content repository for this project
				myRepository = (Repository) dataStore;

				System.out.println("Repository title:" + myRepository.getTitle());
				// System.out.println("Myrepository :" + myRepository);
				break;
			}

			// System.out.println("datastore Title:" + dataStore.getTitle());

		}
		return myRepository;
	}

}
