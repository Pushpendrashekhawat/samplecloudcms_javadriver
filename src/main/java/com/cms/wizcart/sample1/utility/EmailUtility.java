package com.cms.wizcart.sample1.utility;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;

import org.gitana.JSONBuilder;
import org.gitana.platform.client.application.Application;
import org.gitana.platform.client.application.Email;
import org.gitana.platform.client.application.EmailProvider;
import org.gitana.platform.client.attachment.Attachment;
import org.gitana.platform.client.branch.Branch;
import org.gitana.platform.client.node.BaseNode;
import org.gitana.platform.client.platform.Platform;
import org.gitana.platform.support.ResultMap;
import org.gitana.util.JsonUtil;

import com.cms.wizcart.sample1.entities.EmailNotificationPayload;
import com.fasterxml.jackson.databind.node.ObjectNode;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class EmailUtility {
	// TODO: add in property file
	private static final String applicationId = "5f02c86c3fcdad721094";
	Properties properties = new Properties();
	Platform platform = CloudCmsSetup.getConfiguration();
	Branch master = CloudCmsSetup.getBranch();

	public EmailProvider createEmailProvider(String host, String username, String password) {

		Application application = platform.readApplication(applicationId);
		EmailProvider emailProvider = application.createEmailProvider(JSONBuilder.start(EmailProvider.FIELD_HOST)
				.is(host)
				.and(EmailProvider.FIELD_USERNAME)
				.is(username)
				.and(EmailProvider.FIELD_PASSWORD)
				.is(password)
				.and(EmailProvider.FIELD_SMTP_ENABLED)
				.is(true)
				.and(EmailProvider.FIELD_SMTP_IS_SECURE)
				.is(true)
				.and(EmailProvider.FIELD_SMTP_REQUIRES_AUTH)
				.is(true)
				.and(EmailProvider.FIELD_SMTP_STARTTLS_ENABLED)
				.is(true)
				.get());

		return emailProvider;
	}

	// TODO: Confirm if username is "from" and add this as argument to
	// getEmailProvider method
	public EmailProvider getEmailProvider(String username) {
		EmailProvider emailProvider = null;
		if (platform != null) {
			Application application = platform.readApplication(applicationId);
			ObjectNode query = JSONBuilder.start("username")
					.is("pushpendra.singh@avizva.com")
					.get();

			ResultMap<EmailProvider> e = application.queryEmailProviders(query);
			System.out.println(e);
			emailProvider = e.asList()
					.get(0);
			return emailProvider;
		}
		return emailProvider;

	}

	// TODO: Pick "from" from properties file
	public void sendEmail(String to, String from, String emailBody, String subject) {
		Application application = platform.readApplication(applicationId);
		EmailProvider emailProvider = getEmailProvider(from);
		if (platform != null && emailProvider != null) {

			ObjectNode emailObject = JsonUtil.createObject();
			emailObject.put(Email.FIELD_TO, to);
			emailObject.put(Email.FIELD_FROM, from);
			emailObject.put(Email.FIELD_BODY, emailBody);
			emailObject.put(Email.FIELD_SUBJECT, subject);
			Email email = application.createEmail(emailObject);
			emailProvider.send(email);
		}
	}

	public String getEmailBodyFromTemplate(EmailNotificationPayload notificationPayload) throws Exception {

		Configuration configuration = new Configuration(Configuration.VERSION_2_3_30);
		String emailBody = "";
		ObjectNode query = JsonUtil.createObject();
		query.put("title", "Template");
		// TODO: searching can be done on single reguired template also instead
		// of bringing all templates but then we would always need to pass the
		// title of the template always

		ResultMap<BaseNode> resultTemplates = master.searchNodes("Template");
		List<BaseNode> templateNodeList = resultTemplates.asList();
		for (BaseNode templateNode : templateNodeList) {

			List<Attachment> attachments = templateNode.listAttachments()
					.asList();
			// System.out.println("Attachments" + attachments);
			if (attachments.size() > 0) {
				for (Attachment attachment : attachments) {
					var fileName = attachment.getFilename();
					// TODO: Make a enum to pick template file name
					if (fileName.equals("RegistrationVerification.ftl")) {
						try {
							InputStream is = attachment.getInputStream();
							InputStreamReader isr = new InputStreamReader(is);
							Template template = new Template("", isr, configuration);
							StringWriter writer = new StringWriter();
							template.process(notificationPayload.getNotificationData(), writer);
							emailBody = writer.toString();
						} catch (TemplateException | IOException e) {
							throw new Exception(e.getMessage());
						}
					}
				}

			} else
				continue;
		}
		return emailBody;

	}

}
