package com.cms.wizcart.sample_1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.cms.wizcart.sample1.entities.UserDetails;
import com.cms.wizcart.sample1.entities.UserVerificationDetails;
import com.cms.wizcart.sample1.services.UserService;
import com.cms.wizcart.sample1.services.impl.UserServiceImpl;

public class TestQueryOperations {

	UserService userService = new UserServiceImpl();

	@Test
	public void testUserVerification() {
		UserVerificationDetails u = userService.findByUsernameInUserVerification("psingh");
		// System.out.println("Result" + result.asList() + " size" +
		// result.size());
		// assertEquals(true, u != null);
		assertEquals("no", u.getIsVerified());
	}

	@Test
	public void testUserVerification2() {
		UserVerificationDetails u = userService.findByUsernameInUserVerification("psingh");
		// System.out.println("Result" + result.asList() + " size" +
		// result.size());
		// assertEquals(true, u != null);
		assertEquals("no", u.getIsVerified());
	}

	@Test
	public void testUserVerification3() {
		UserDetails u = userService.findByUsernameInUsers("richardh");
		// System.out.println("Result" + result.asList() + " size" +
		// result.size());
		System.out.println(u.getFirstName());
		// assertEquals(true, u != null);
		assertEquals("no", u.getNotificationConsent());
	}

}
