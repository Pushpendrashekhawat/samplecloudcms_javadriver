package com.cms.wizcart.sample_1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.cms.wizcart.sample1.services.VerificationCodeService;
import com.cms.wizcart.sample1.services.impl.VerificationCodeServiceImpl;

public class TestValidateCode {

	@Test
	public void TestValidateCode1() {

		VerificationCodeService verificationCodeService = new VerificationCodeServiceImpl();
		boolean result = verificationCodeService.validateCode(12332, "psingh");
		System.out.println(result);
		assertEquals(false, result);
	}

	@Test
	public void TestValidateCode2() {

		VerificationCodeService verificationCodeService = new VerificationCodeServiceImpl();
		boolean result = verificationCodeService.validateCode(436426, "psingh");
		System.out.println(result);
		assertEquals(true, result);
	}

	// This test should fail since the user is already verifies now
	@Test
	public void TestValidateCode3() {

		VerificationCodeService verificationCodeService = new VerificationCodeServiceImpl();
		boolean result = verificationCodeService.validateCode(436426, "psingh");
		System.out.println(result);
		assertEquals(true, result);
	}
}
