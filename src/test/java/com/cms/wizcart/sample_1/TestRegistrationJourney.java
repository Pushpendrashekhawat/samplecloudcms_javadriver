package com.cms.wizcart.sample_1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.cms.wizcart.sample1.entities.NotificationConsent;
import com.cms.wizcart.sample1.entities.UserDetails;
import com.cms.wizcart.sample1.services.UserOnboardingJourneyService;
import com.cms.wizcart.sample1.services.impl.UserOnboardingJourneyServiceImpl;

public class TestRegistrationJourney {

	UserOnboardingJourneyService userOnboardingJourneyService = new UserOnboardingJourneyServiceImpl();

	@Test
	public void testDataInsertion1() throws Exception {
		UserDetails userDetails = new UserDetails("John", "Wick", "johnw", "test@123", NotificationConsent.yes, "johnw@mailinator.com", "8080808080", "custom:customer", "");
		Boolean isNodeSaved = userOnboardingJourneyService.RegisterCustomer(userDetails);
		// Boolean isNodeSaved =
		// userOnboardingJourneyService.RegisterCustomer("John", "Wick",
		// "johnw", "test@123", "yes", "johnw@mailinator.com", "8080808080",
		// "custom:customer");
		assertEquals(true, isNodeSaved);

	}

	@Test
	public void testDataInsertion2() throws Exception {
		UserDetails userDetails = new UserDetails("Richard", "henry", "richardh", "test@123", NotificationConsent.yes, "richardh@mailinator.com", "8080808080", "custom:customer", "");
		Boolean isNodeSaved = userOnboardingJourneyService.RegisterCustomer(userDetails);

		assertEquals(true, isNodeSaved);
	}

	// test should fail if username already exists
	@Test
	public void testUniqueUsername() throws Exception {
		UserDetails userDetails = new UserDetails("Richard", "henry", "richardh", "test@123", NotificationConsent.yes, "richardh@mailinator.com", "8080808080", "custom:customer", "");
		Boolean isNodeSaved = userOnboardingJourneyService.RegisterCustomer(userDetails);

		// assertFalse(isNodeSaved);
		assertEquals(true, isNodeSaved);
	}

	@Test
	public void testUniqueEmail() throws Exception {

		UserDetails userDetails = new UserDetails("Richard", "smith", "richards", "test@123", NotificationConsent.yes, "richardh@mailinator.com", "8080808080", "custom:customer", "");
		Boolean isNodeSaved = userOnboardingJourneyService.RegisterCustomer(userDetails);

		// assertFalse(isNodeSaved);
		assertEquals(true, isNodeSaved);
	}

	@Test
	public void testDataInsertion3() throws Exception {
		UserDetails userDetails = new UserDetails("Ross", "wick", "rosswick", "Test@123", NotificationConsent.yes, "rosswick@mailinator.com", "8080808080", "custom:customer", "");
		Boolean isNodeSaved = userOnboardingJourneyService.RegisterCustomer(userDetails);
		assertEquals(true, isNodeSaved);
	}

	// registering user with title
	@Test
	public void testDataInsertion4() throws Exception {
		UserDetails userDetails = new UserDetails("Ross", "Williams", "rossw", "Test@123", NotificationConsent.yes, "rossw@mailinator.com", "8080808080", "custom:customer", "Ross Willians");
		Boolean isNodeSaved = userOnboardingJourneyService.RegisterCustomer(userDetails);
		assertEquals(true, isNodeSaved);

	}

	@Test
	public void testDataInsertion5() throws Exception {
		UserDetails userDetails = new UserDetails("Pushpendra", "Singh", "psingh", "Test@123", NotificationConsent.yes, "shekhawatpushpendra9@gmail.com", "8080808080", "custom:customer", "Pushpendra Singh");
		Boolean isNodeSaved = userOnboardingJourneyService.RegisterCustomer(userDetails);
		assertEquals(true, isNodeSaved);

	}

	// username should be Unique
	// username should be case Sensitive

	// password should contain more than 6 characters
	// verify that the password is encrypted

	// send verificationEmail to entered email ID
	// email should have @ mandatorily
	// email should be unique

	// Phone number should be numerix

	// All the fields should be mandatory

}
