package com.cms.wizcart.sample_1;

import static org.junit.Assert.assertEquals;

import org.gitana.platform.support.ResultMap;
import org.junit.Test;

import com.cms.wizcart.sample1.services.UserOnboardingJourneyService;
import com.cms.wizcart.sample1.services.impl.UserOnboardingJourneyServiceImpl;

public class TestAuthenticationJourney {

	// UserOnboardingJourney customerOnboardingJourney = new
	// UserOnboardingJourney();
	UserOnboardingJourneyService userOnboardingJourneyService = new UserOnboardingJourneyServiceImpl();

	@Test
	public void test1() {

		ResultMap result = userOnboardingJourneyService.AuthenticateCustomer("test", "test");
		System.out.println("User details" + result);
		assertEquals(1, result.size());
	}

	// passing invalid password for username test : testFails
	@Test
	public void test2() {

		ResultMap result = userOnboardingJourneyService.AuthenticateCustomer("test", "tessst");
		assertEquals(1, result.size());

	}

	@Test
	public void test3() {

		ResultMap result = userOnboardingJourneyService.AuthenticateCustomer("johnkd", "test@123");
		System.out.println("User details for test 3" + result);
		assertEquals(1, result.size());
	}

	@Test
	public void test4() {

		ResultMap result = userOnboardingJourneyService.AuthenticateCustomer("richardh", "test@123");
		System.out.println("User details for test 4" + result);
		assertEquals(1, result.size());
	}

	@Test
	public void test5() {

		ResultMap result = userOnboardingJourneyService.AuthenticateCustomer("psingh", "test@123");
		System.out.println("User details for test 5" + result);
		assertEquals(1, result.size());
	}

}
