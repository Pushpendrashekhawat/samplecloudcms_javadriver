package com.cms.wizcart.sample_1;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.gitana.JSONBuilder;
import org.gitana.platform.client.application.Application;
import org.gitana.platform.client.application.EmailProvider;
import org.gitana.platform.client.attachment.Attachment;
import org.gitana.platform.client.branch.Branch;
import org.gitana.platform.client.domain.Domain;
import org.gitana.platform.client.node.BaseNode;
import org.gitana.platform.client.platform.Platform;
import org.gitana.platform.support.ResultMap;
import org.gitana.util.JsonUtil;
import org.junit.Test;

import com.cms.wizcart.sample1.utility.CloudCmsSetup;
import com.cms.wizcart.sample1.utility.EmailUtility;
import com.fasterxml.jackson.databind.node.ObjectNode;

import freemarker.template.Configuration;
import freemarker.template.Template;

// This class will we removed, it was made to test the concepts of emailProvider
// and also email.
public class TestEmailProvider {
	Platform platform = CloudCmsSetup.getConfiguration();

	@Test
	public void test() {

		// ObjectNode query = QueryBuilder.start("Application Title")
		// .is("Service Marketplace Web Application")
		// .get();
		// ResultMap<Application> resultMap = platform.queryApplications(query);
		// System.out.println(resultMap);

		ResultMap<Application> resultMap = platform.listApplications();

		List<Application> list = resultMap.asList();
		List<EmailProvider> EmailProviders = new ArrayList<EmailProvider>();
		// for (Application appl : list) {
		//
		// Email email = appl.createEmail(
		// JSONBuilder.start(Email.FIELD_TO).is("buildtest@gitanasoftware.com").and(Email.FIELD_BODY)
		// .is("Here is a test
		// body").and(Email.FIELD_FROM).is("buildtest@gitanasoftware.com").get());
		//
		// }
		// 5f02c86c3fcdad721094
		Application app = platform.readApplication("5f02c86c3fcdad721094");

		Domain d = platform.readPrimaryDomain();
		List<Application> applicationList = platform.listApplications()
				.asList();
		for (Application application : applicationList) {
			var appId = application.getId();
			System.out.println("APplication description:" + appId + " " + application.getDescription());
			System.out.println("Emails for application ID:" + appId + " " + application.listEmails());
			System.out.println("EMAIL providers for application ID:" + appId + " " + application.listEmailProviders());
		}

		// System.out.println("EMAIL providers for application ID:" +
		// app.listEmailProviders());
		// System.out.println("Emails for application ID:" + app.listEmails());
		EmailProvider emailProvider = app.createEmailProvider(JSONBuilder.start(EmailProvider.FIELD_HOST)
				.is("smtp.gmail.com")
				.and(EmailProvider.FIELD_USERNAME)
				.is("pushpendra.singh@avizva.com")
				.and(EmailProvider.FIELD_PASSWORD)
				.is("Rj075757")
				.and(EmailProvider.FIELD_SMTP_ENABLED)
				.is(true)
				.and(EmailProvider.FIELD_SMTP_IS_SECURE)
				.is(true)
				.and(EmailProvider.FIELD_SMTP_REQUIRES_AUTH)
				.is(true)
				.and(EmailProvider.FIELD_SMTP_STARTTLS_ENABLED)
				.is(true)
				.get());
		// assertEquals(true, emailProvider.isSaved());

		// Node node = (Node) NodeBuilder.start("username")
		// .is("");
		// node.getId();
		// Email email1 = app.createEmail(JSONBuilder.start(Email.FIELD_TO)
		// .is("user@mailinator.com")
		// .and(Email.FIELD_BODY)
		// .is("Here is a test body")
		// .and(Email.FIELD_FROM)
		// .is("user1@user.com")
		// .and(Email.FIELD_BODY_NODE_ID)
		// .is(node.getId())
		// .get());

		// a5c20c08f2e2010f6c24
		// System.out.println("LIST OF APPLICATIONS: " +
		// platform.listApplications() +
		// "SIZE: " + platform.listApplications().size()); ;
		// System.out.println("EMAIL PROVIDERS:" + app.listEmailProviders() +
		// "SIZE:" +
		// app.listEmailProviders().size());
		// app.readEmailProvider(emailProviderId);
		// System.out.println("Primary DOMAIN NAME: " +
		// platform.readPrimaryDomain());
		//
		// ResultMap<EmailProvider> emailProviderMap =
		// app.queryEmailProviders(JSONBuilder.start("username")
		// .is("pushpendra.singh@avizva.com")
		// .get());
		// System.out.println(emailProviderMap);
		// assertEquals(1, emailProviderMap.size());
		// Email email = app
		// .createEmail(JSONBuilder.start(Email.FIELD_TO).is("pushpendra.sing@avizva.com").and(Email.FIELD_BODY)
		// .is("Here is a test
		// body").and(Email.FIELD_FROM).is("shekhawatpushpendra9@gmail.com").get());
		//
		// if (emailProviderMap.asList().iterator().hasNext()) {
		// EmailProvider e = emailProviderMap.asList().iterator().next();
		// e.send(email);
		// }
		//
		// email.reload();
		// assertTrue(email.getSent());
		// assertNotNull(email.dateSent());
		// assertNotNull(email.getSentBy());
		// resultMap.asList().forEach(application -> System.out.println(
		// "TITLE DESCRIPTION " + application.getId()+ "EMAIL PROVIDERS" +
		// application.listEmailProviders())
		//
		// );

	}

	@Test
	public void Tesrrt() {
		Configuration configuration = new Configuration();
		// TODO: make template type enum and specify their values
		Branch master = CloudCmsSetup.getBranch();
		ObjectNode query = JsonUtil.createObject();
		query.put("title", "Template");
		// query.put("contentType", "application/freemarker");
		ResultMap<BaseNode> results = master.searchNodes("Template");
		// System.out.println(results + "RESULTS SIZE:" + results.size());

		results.asList()
				.forEach(result -> {
					System.out.println("Entering here");
					ResultMap<Attachment> attachments = result.listAttachments();

					System.out.println(attachments.size());
					attachments.asList()
							.forEach(at -> {
								// System.out.println("CONTENTTYPE:" +
								// at.getContentType());
								var fileName = at.getFilename();
								System.out.println("FILENAME:" + fileName);
								if (fileName.equals("RegistrationVerification.ftl")) {
									System.out.println("FileName Matched");
									InputStream is = at.getInputStream();
									InputStreamReader isr = new InputStreamReader(is);
									try {
										Template template = new Template("", isr, configuration);
										StringWriter writer = new StringWriter();
										// template.process(dataModel, writer);
									} catch (IOException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
									var atId = at.getId();
									String res;
									try {
										res = IOUtils.toString(is);
										System.out.println(res);
									} catch (IOException e) {

										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}

							});
					// result.downloadAttachment();
				});

		// InputStream ix = new InputStreamBody(results.downloadAttachment(),
		// ContentType.MULTIPART_FORM_DATA);

	}

	@Test
	public void TestEmailProvider() {
		Properties properties = new Properties();
		EmailUtility emailUtility = new EmailUtility();
		List<Application> applist = platform.listApplications()
				.asList();
		// System.out.println("Application list" + applist);
		try {
			properties.load(new FileInputStream("wizcart-sample_1/src/main/resources/application.properties"));
			String a = properties.getProperty("email.from");
			System.out.println(a);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// EmailProvider emailProvider = emailUtility.getEmailProvider();
		// System.out.println(emailProvider);

	}

}
