package com.cms.wizcart.sample_1;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.gitana.platform.client.application.EmailProvider;
import org.gitana.platform.client.platform.Platform;
import org.junit.Test;

import com.cms.wizcart.sample1.entities.EmailNotificationPayload;
import com.cms.wizcart.sample1.services.VerificationCodeService;
import com.cms.wizcart.sample1.services.impl.VerificationCodeServiceImpl;
import com.cms.wizcart.sample1.utility.CloudCmsSetup;
import com.cms.wizcart.sample1.utility.EmailUtility;

public class TestEmail {
	Platform platform = CloudCmsSetup.getConfiguration();

	@Test
	public void TestEmailProvider() {
		EmailUtility emailUtility = new EmailUtility();
		EmailProvider emailProvider = emailUtility.getEmailProvider("pushpendra.singh@avizva.com");
		System.out.println(emailProvider);
	}

	@Test
	public void TestEmailBody() throws Exception {
		EmailUtility emailUtility = new EmailUtility();
		EmailNotificationPayload payload = new EmailNotificationPayload();

		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("verificationCode", 123456);

		payload.setNotificationData(dataMap);

		String emailBody = emailUtility.getEmailBodyFromTemplate(payload);
		System.out.println(emailBody);
	}

	@Test
	public void TestSendVerificationCode() {
		VerificationCodeService verificationCodeService = new VerificationCodeServiceImpl();

		var isEmailSent = verificationCodeService.sendVerificationCode("psingh", "shekhawatpushpendra9@gmail.com");
		assertEquals(true, isEmailSent);
	}

}
